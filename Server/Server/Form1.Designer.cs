﻿namespace Server
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.openFileDialog = new System.Windows.Forms.OpenFileDialog();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.playlist_location_tb = new System.Windows.Forms.TextBox();
            this.tables_location_tb = new System.Windows.Forms.TextBox();
            this.tables_browse_b = new System.Windows.Forms.Button();
            this.playlist_brouse_b = new System.Windows.Forms.Button();
            this.contest_data_gb = new System.Windows.Forms.GroupBox();
            this.get_data_b = new System.Windows.Forms.Button();
            this.progressBar1 = new System.Windows.Forms.ProgressBar();
            this.server_gb = new System.Windows.Forms.GroupBox();
            this.server_port = new System.Windows.Forms.NumericUpDown();
            this.label8 = new System.Windows.Forms.Label();
            this.clients_tb = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.server_status = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.stop_server_b = new System.Windows.Forms.Button();
            this.start_server_b = new System.Windows.Forms.Button();
            this.contest_gb = new System.Windows.Forms.GroupBox();
            this.group_nud = new System.Windows.Forms.NumericUpDown();
            this.round_nud = new System.Windows.Forms.NumericUpDown();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.pause_contest_b = new System.Windows.Forms.Button();
            this.start_contest_b = new System.Windows.Forms.Button();
            this.contest_data_gb.SuspendLayout();
            this.server_gb.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.server_port)).BeginInit();
            this.contest_gb.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.group_nud)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.round_nud)).BeginInit();
            this.SuspendLayout();
            // 
            // openFileDialog
            // 
            this.openFileDialog.FileName = "openFileDialog1";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 29);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(42, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Tables:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(6, 57);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(42, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Playlist:";
            // 
            // playlist_location_tb
            // 
            this.playlist_location_tb.Location = new System.Drawing.Point(54, 54);
            this.playlist_location_tb.Name = "playlist_location_tb";
            this.playlist_location_tb.ReadOnly = true;
            this.playlist_location_tb.Size = new System.Drawing.Size(211, 20);
            this.playlist_location_tb.TabIndex = 2;
            // 
            // tables_location_tb
            // 
            this.tables_location_tb.Location = new System.Drawing.Point(54, 26);
            this.tables_location_tb.Name = "tables_location_tb";
            this.tables_location_tb.ReadOnly = true;
            this.tables_location_tb.Size = new System.Drawing.Size(211, 20);
            this.tables_location_tb.TabIndex = 3;
            // 
            // tables_browse_b
            // 
            this.tables_browse_b.Location = new System.Drawing.Point(272, 24);
            this.tables_browse_b.Name = "tables_browse_b";
            this.tables_browse_b.Size = new System.Drawing.Size(75, 23);
            this.tables_browse_b.TabIndex = 4;
            this.tables_browse_b.Text = "Browse";
            this.tables_browse_b.UseVisualStyleBackColor = true;
            this.tables_browse_b.Click += new System.EventHandler(this.tables_browse_b_Click);
            // 
            // playlist_brouse_b
            // 
            this.playlist_brouse_b.Location = new System.Drawing.Point(271, 52);
            this.playlist_brouse_b.Name = "playlist_brouse_b";
            this.playlist_brouse_b.Size = new System.Drawing.Size(75, 23);
            this.playlist_brouse_b.TabIndex = 5;
            this.playlist_brouse_b.Text = "Browse";
            this.playlist_brouse_b.UseVisualStyleBackColor = true;
            this.playlist_brouse_b.Click += new System.EventHandler(this.playlist_brouse_b_Click);
            // 
            // contest_data_gb
            // 
            this.contest_data_gb.Controls.Add(this.get_data_b);
            this.contest_data_gb.Controls.Add(this.progressBar1);
            this.contest_data_gb.Controls.Add(this.label1);
            this.contest_data_gb.Controls.Add(this.playlist_brouse_b);
            this.contest_data_gb.Controls.Add(this.label2);
            this.contest_data_gb.Controls.Add(this.playlist_location_tb);
            this.contest_data_gb.Controls.Add(this.tables_browse_b);
            this.contest_data_gb.Controls.Add(this.tables_location_tb);
            this.contest_data_gb.Location = new System.Drawing.Point(12, 12);
            this.contest_data_gb.Name = "contest_data_gb";
            this.contest_data_gb.Size = new System.Drawing.Size(354, 150);
            this.contest_data_gb.TabIndex = 6;
            this.contest_data_gb.TabStop = false;
            this.contest_data_gb.Text = "Contest data";
            // 
            // get_data_b
            // 
            this.get_data_b.Location = new System.Drawing.Point(143, 119);
            this.get_data_b.Name = "get_data_b";
            this.get_data_b.Size = new System.Drawing.Size(75, 23);
            this.get_data_b.TabIndex = 7;
            this.get_data_b.Text = "OK";
            this.get_data_b.UseVisualStyleBackColor = true;
            this.get_data_b.Click += new System.EventHandler(this.get_data_b_Click);
            // 
            // progressBar1
            // 
            this.progressBar1.Location = new System.Drawing.Point(9, 90);
            this.progressBar1.Maximum = 10000;
            this.progressBar1.Name = "progressBar1";
            this.progressBar1.Size = new System.Drawing.Size(337, 23);
            this.progressBar1.TabIndex = 6;
            // 
            // server_gb
            // 
            this.server_gb.Controls.Add(this.server_port);
            this.server_gb.Controls.Add(this.label8);
            this.server_gb.Controls.Add(this.clients_tb);
            this.server_gb.Controls.Add(this.label7);
            this.server_gb.Controls.Add(this.server_status);
            this.server_gb.Controls.Add(this.label5);
            this.server_gb.Controls.Add(this.stop_server_b);
            this.server_gb.Controls.Add(this.start_server_b);
            this.server_gb.Location = new System.Drawing.Point(12, 253);
            this.server_gb.Name = "server_gb";
            this.server_gb.Size = new System.Drawing.Size(354, 106);
            this.server_gb.TabIndex = 7;
            this.server_gb.TabStop = false;
            this.server_gb.Text = "Server";
            // 
            // server_port
            // 
            this.server_port.Location = new System.Drawing.Point(224, 24);
            this.server_port.Maximum = new decimal(new int[] {
            65535,
            0,
            0,
            0});
            this.server_port.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.server_port.Name = "server_port";
            this.server_port.Size = new System.Drawing.Size(122, 20);
            this.server_port.TabIndex = 6;
            this.server_port.Value = new decimal(new int[] {
            703,
            0,
            0,
            0});
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(179, 26);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(29, 13);
            this.label8.TabIndex = 6;
            this.label8.Text = "Port:";
            // 
            // clients_tb
            // 
            this.clients_tb.Location = new System.Drawing.Point(54, 46);
            this.clients_tb.Name = "clients_tb";
            this.clients_tb.ReadOnly = true;
            this.clients_tb.Size = new System.Drawing.Size(292, 20);
            this.clients_tb.TabIndex = 5;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(6, 49);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(41, 13);
            this.label7.TabIndex = 4;
            this.label7.Text = "Clients:";
            // 
            // server_status
            // 
            this.server_status.AutoSize = true;
            this.server_status.Location = new System.Drawing.Point(51, 26);
            this.server_status.Name = "server_status";
            this.server_status.Size = new System.Drawing.Size(45, 13);
            this.server_status.TabIndex = 3;
            this.server_status.Text = "stopped";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(6, 26);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(40, 13);
            this.label5.TabIndex = 2;
            this.label5.Text = "Status:";
            // 
            // stop_server_b
            // 
            this.stop_server_b.Enabled = false;
            this.stop_server_b.Location = new System.Drawing.Point(179, 72);
            this.stop_server_b.Name = "stop_server_b";
            this.stop_server_b.Size = new System.Drawing.Size(167, 23);
            this.stop_server_b.TabIndex = 1;
            this.stop_server_b.Text = "Stop server";
            this.stop_server_b.UseVisualStyleBackColor = true;
            this.stop_server_b.Click += new System.EventHandler(this.stop_server_b_Click);
            // 
            // start_server_b
            // 
            this.start_server_b.Location = new System.Drawing.Point(9, 72);
            this.start_server_b.Name = "start_server_b";
            this.start_server_b.Size = new System.Drawing.Size(167, 23);
            this.start_server_b.TabIndex = 0;
            this.start_server_b.Text = "Start server";
            this.start_server_b.UseVisualStyleBackColor = true;
            this.start_server_b.Click += new System.EventHandler(this.start_server_b_Click);
            // 
            // contest_gb
            // 
            this.contest_gb.Controls.Add(this.group_nud);
            this.contest_gb.Controls.Add(this.round_nud);
            this.contest_gb.Controls.Add(this.label4);
            this.contest_gb.Controls.Add(this.label3);
            this.contest_gb.Controls.Add(this.pause_contest_b);
            this.contest_gb.Controls.Add(this.start_contest_b);
            this.contest_gb.Location = new System.Drawing.Point(12, 168);
            this.contest_gb.Name = "contest_gb";
            this.contest_gb.Size = new System.Drawing.Size(354, 77);
            this.contest_gb.TabIndex = 8;
            this.contest_gb.TabStop = false;
            this.contest_gb.Text = "Contest";
            // 
            // group_nud
            // 
            this.group_nud.Location = new System.Drawing.Point(224, 18);
            this.group_nud.Name = "group_nud";
            this.group_nud.Size = new System.Drawing.Size(122, 20);
            this.group_nud.TabIndex = 5;
            this.group_nud.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // round_nud
            // 
            this.round_nud.Location = new System.Drawing.Point(54, 18);
            this.round_nud.Name = "round_nud";
            this.round_nud.Size = new System.Drawing.Size(122, 20);
            this.round_nud.TabIndex = 4;
            this.round_nud.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(179, 20);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(39, 13);
            this.label4.TabIndex = 3;
            this.label4.Text = "Group:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(9, 20);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(42, 13);
            this.label3.TabIndex = 2;
            this.label3.Text = "Round:";
            // 
            // pause_contest_b
            // 
            this.pause_contest_b.Location = new System.Drawing.Point(180, 44);
            this.pause_contest_b.Name = "pause_contest_b";
            this.pause_contest_b.Size = new System.Drawing.Size(167, 23);
            this.pause_contest_b.TabIndex = 1;
            this.pause_contest_b.Text = "Pause contest";
            this.pause_contest_b.UseVisualStyleBackColor = true;
            this.pause_contest_b.Click += new System.EventHandler(this.pause_contest_b_Click);
            // 
            // start_contest_b
            // 
            this.start_contest_b.Location = new System.Drawing.Point(9, 44);
            this.start_contest_b.Name = "start_contest_b";
            this.start_contest_b.Size = new System.Drawing.Size(167, 23);
            this.start_contest_b.TabIndex = 0;
            this.start_contest_b.Text = "Start contest";
            this.start_contest_b.UseVisualStyleBackColor = true;
            this.start_contest_b.Click += new System.EventHandler(this.start_contest_b_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(380, 371);
            this.Controls.Add(this.contest_gb);
            this.Controls.Add(this.server_gb);
            this.Controls.Add(this.contest_data_gb);
            this.Name = "Form1";
            this.Text = "F3K Server";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Form1_FormClosing);
            this.contest_data_gb.ResumeLayout(false);
            this.contest_data_gb.PerformLayout();
            this.server_gb.ResumeLayout(false);
            this.server_gb.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.server_port)).EndInit();
            this.contest_gb.ResumeLayout(false);
            this.contest_gb.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.group_nud)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.round_nud)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.OpenFileDialog openFileDialog;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox playlist_location_tb;
        private System.Windows.Forms.TextBox tables_location_tb;
        private System.Windows.Forms.Button tables_browse_b;
        private System.Windows.Forms.Button playlist_brouse_b;
        private System.Windows.Forms.GroupBox contest_data_gb;
        private System.Windows.Forms.Button get_data_b;
        private System.Windows.Forms.ProgressBar progressBar1;
        private System.Windows.Forms.GroupBox server_gb;
        private System.Windows.Forms.TextBox clients_tb;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label server_status;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button stop_server_b;
        private System.Windows.Forms.Button start_server_b;
        private System.Windows.Forms.GroupBox contest_gb;
        private System.Windows.Forms.NumericUpDown group_nud;
        private System.Windows.Forms.NumericUpDown round_nud;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button pause_contest_b;
        private System.Windows.Forms.Button start_contest_b;
        private System.Windows.Forms.NumericUpDown server_port;
        private System.Windows.Forms.Label label8;
    }
}

