package com.example.myclient;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.SocketAddress;
import java.util.StringTokenizer;

import android.os.Handler;
import android.util.Log;

public class TcpClient {
	
	private static TcpClient tcpClient;
	
	private static Handler handler = null;
	
	private String serverMessage;
	private static String ipAddress;
	private static int port;
	private static OnMessageReceived mMessageListener = null;
	private boolean mRun = false;

	static PrintWriter out;
	BufferedReader in;
	
	private TcpClient()
	{}
	
	public static TcpClient createTcpClient(OnMessageReceived listener)
	{
		if(tcpClient == null) tcpClient = new TcpClient();
		mMessageListener = listener;
		return tcpClient;		
	}
	
	
	public static boolean checkIpAddress(String ip)
	{
	    	StringTokenizer tokens = new StringTokenizer(ip,".");
	    	
	    	if(tokens.countTokens() != 4) return false;
	    	
	    	int num;
	    	for(int i=0; i<4; i++)
	    	{
	    		try
	    		{
	    			num = Integer.parseInt(tokens.nextToken());
	    			if(num < 0 || num > 255) return false;
	    		}
	    		catch(NumberFormatException ex)
	    		{
	    			return false;
	    		}    		
	    	}
	    	return true;
	}
	
	public static boolean setServerAddress(String ip, int p)
	{
		if(!checkIpAddress(ip)) return false;
		port = p;
		ipAddress = ip;
		return true;
	}
	
	public static boolean sendMessage(String message)
	{
		if(out != null && !out.checkError() && message != null && message != "")
		{
			out.println(message);
			out.flush();
			return true;
		}
		return false;
	}
	
	public void stopClient()
	{
		if(mRun){
		mRun = false;
		sendMessage("Logoff*"+ContestInfo.getCurNum());
		if(out != null) out.close();
		out = null;
		try {
			if(in != null) in.close();
			in = null;
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		}
	}
	
	public static void setHandler(Handler h)
	{
		handler = h;
	}
	
	public void setMessageReceiver(OnMessageReceived listener)
	{
		Log.e("", "seting in client");
		TcpClient.mMessageListener = listener;
		Log.e("","set in client");
	}
	
	public void removeHandler()
	{
		handler = null;
	}
	
	public void run()
	{
		mRun = true;
		
		try {
            //here you must put your computer's IP address.
            InetAddress serverAddr = InetAddress.getByName(ipAddress);
 
            Log.e("TCP Client", "C: Connecting...");
            if(handler != null) handler.sendEmptyMessage(CONNECTING_MESSAGE);
 
            
            //create a socket to make the connection with the server
            //Socket socket = new Socket(serverAddr, port);
            Socket socket = new Socket();
            SocketAddress socketAddr = new InetSocketAddress(serverAddr,port);
            socket.connect(socketAddr, 5000);
            try {
 
                //send the message to the server
                out = new PrintWriter(new BufferedWriter(new OutputStreamWriter(socket.getOutputStream())), true);
 
                if(handler != null) handler.sendEmptyMessage(CONNECTED_MESSAGE);
                
                //receive the message which the server sends back
                in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
 
                //in this while the client listens for the messages sent by the server
                while (mRun) {
                    serverMessage = in.readLine();
                    
                    if (serverMessage != null && mMessageListener != null) {
                        //call the method messageReceived from MyActivity class
                        mMessageListener.messageReceived(serverMessage);
                      //  Log.e("RESPONSE FROM SERVER", "S: Received Message: '" + serverMessage + "'");
                    }
                    serverMessage = null; 
                }
 
                
 
            } catch (Exception e) {
 
                Log.e("in", e.getMessage());
                if(handler != null) handler.sendEmptyMessage(CONNECTION_ERROR);
 
            } finally {
                //the socket must be closed. It is not possible to reconnect to this socket
                // after it is closed, which means a new socket instance has to be created.
                socket.close();
                if(handler != null) handler.sendEmptyMessage(CLOSE_CONNECTION);
            }
 
        } catch (Exception e) {
 
        	 Log.e("out", e.getMessage()); 
        	 if(handler != null) handler.sendEmptyMessage(CONNECTION_ERROR);
        }
		
		
	}
	
	public interface OnMessageReceived {
        public void messageReceived(String message);
    }
	
	public static final int CONNECTING_MESSAGE = 0;
	public static final int CONNECTED_MESSAGE = 1;
	public static final int CONNECTION_ERROR = 2;
	public static final int RECEIVING_DATA = 3;
	public static final int DATA_RECEIVED = 4;
	public static final int CONTEST_IS_RUNNING = 5;
	public static final int CLOSE_CONNECTION = 6;
}
