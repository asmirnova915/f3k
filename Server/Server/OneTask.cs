﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Server
{
    public class OneTask
    {
        private String name;
        private String task;
        private int round;
        private int judgeNum;
        private String group;

        public OneTask(String n, String t, int r, int jN, String g)
        {
            name = n.Clone().ToString();
            task = t.Clone().ToString();
            round = r;
            judgeNum = jN;
            group = g.Clone().ToString();
        }

        public String ToString()
        {
            String rez = round.ToString() + "|" + task + "|" + group + "|" + name + "|" + judgeNum.ToString();
            return rez;
        }

        public int getJudgeNum()
        {
            return judgeNum;
        }
    }
}
