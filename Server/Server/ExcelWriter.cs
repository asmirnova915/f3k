﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Microsoft.Office.Interop.Excel;

namespace Server
{
    class ExcelWriter
    {
        Application objExcel;
        Workbook objWorkbook;
        String file;

        public ExcelWriter(String file)
        {
            objExcel = GetApplication();
            objWorkbook = objExcel.Workbooks.Open(file);
            objExcel.Visible = true;
            this.file = file;
        }

        Application GetApplication()
        {
            Application excel = null;
            try
            {
                excel = System.Runtime.InteropServices.Marshal.GetActiveObject("Excel.Application") as Application;
            }
            catch (Exception ex)
            {
                excel = new Application();
            }
            return excel;
        }

        public void writeResult(String message)
        {
            String[] split = message.Split(new Char[] { '|' });
            try
            {
                int curNum = Convert.ToInt32(split[1]);
                int curRound = Convert.ToInt32(split[2]);
                
                objExcel.Visible = true;
                
                Worksheet objWorksheet = objWorkbook.Sheets[3 + curRound];
                String curName = split[3];
                String curTask = split[4];
                int rowNum = -1;
                for (int i = 1; i < objWorksheet.Rows.Count; i++) 
                {
                    if(((objWorksheet.Cells[i, 1] as Range).Value2) == curName)
                    {
                        rowNum = i;
                        break;
                    }
                }
                for(int i=5; i<split.Length; i++)
                {
                    (objWorksheet.Cells[rowNum, i - 3] as Range).Value2 = split[i];
                }
                
            }
            catch (Exception ex)
            {
                System.Windows.Forms.MessageBox.Show(ex.Message); 
            }
            
        }

        public void writePenalty(String message)
        {
            String[] split = message.Split(new Char[] { '|' });
            try
            {
                int curNum = Convert.ToInt32(split[1]);
                int curRound = Convert.ToInt32(split[2]);

                objExcel.Visible = true;

                Worksheet objWorksheet = objWorkbook.Sheets[3 + curRound];
                String curName = split[3];
                int rowNum = -1;
                for (int i = 1; i < objWorksheet.Rows.Count; i++)
                {
                    if (((objWorksheet.Cells[i, 1] as Range).Value2) == curName)
                    {
                        rowNum = i;
                        break;
                    }
                }
                int columnNum = 1;
                for(int i = 1; i<objWorksheet.Columns.Count; i++)
                {
                    if(((objWorksheet.Cells[1, i] as Range).Value2) == "Penalty")
                    {
                        columnNum = i;
                        break;
                    }
                }
                (objWorksheet.Cells[rowNum, columnNum] as Range).Value2 = split[4];
            }
            catch (Exception ex)
            {
                System.Windows.Forms.MessageBox.Show(ex.Message);
            }

        }

        public void CloseBook()
        {
            try
            {
                objWorkbook.Save();
            }
            catch (Exception ex)
            {

            }
            finally
            {
                objExcel.Quit();
            }
        }

    }
}
