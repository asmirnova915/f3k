package com.example.myclient;

import android.app.Dialog;
import android.app.DialogFragment;
import android.app.TimePickerDialog;
import android.os.Bundle;
import android.text.format.DateFormat;
import android.widget.TimePicker;

public class TimePickerFragment extends DialogFragment
						implements TimePickerDialog.OnTimeSetListener {

	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState) {
		
		int hour = ContestInfo.target / 60;
		int minute = ContestInfo.target % 60;

		// Create a new instance of TimePickerDialog and return it
		return new TimePickerDialog(getActivity(), this, hour, minute,
					DateFormat.is24HourFormat(getActivity()));
	}

	public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
		ContestInfo.target = hourOfDay * 60 + minute;
		TimerFragment.setTargetTime(ContestInfo.target);
		if(ContestInfo.target != 0) TimerFragment.setStartButtonEnabled(true); 
	}
	
}
