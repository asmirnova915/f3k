package com.example.myclient;

import java.util.ArrayList;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnCancelListener;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.Gravity;
import android.widget.Toast;

public class PenaltyList extends DialogFragment{
	
	final CharSequence[] items = {" Contact with person "," Forbidden airspace "," Contact in safety area "," Out of time flight "};
	ArrayList selectedItems;
	AlertDialog.Builder builder;
	static public String name;
	static public String round;
	Context context;
	int duration;
	Toast error;
	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState) {
		
		context = getActivity().getApplicationContext();
		duration = Toast.LENGTH_SHORT;
		
		builder = new AlertDialog.Builder(getActivity());
        builder.setTitle("Penalty");
        selectedItems = new ArrayList();
        
        builder.setMultiChoiceItems(items, null, new DialogInterface.OnMultiChoiceClickListener() {
			
			@Override
			public void onClick(DialogInterface dialog, int which, boolean isChecked) {
				if(isChecked){
					selectedItems.add(which);
				}
				else if(selectedItems.contains(which)){
					selectedItems.remove(Integer.valueOf(which));
				}
			}
		});
        
        builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
			
			@Override
			public void onClick(DialogInterface dialog, int which) {
				builder.setTitle("Are you sure?");  
				builder.setMessage("Are you sure you want to send penalty to server?"); 
				builder.setPositiveButton("Yes", new android.content.DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int arg1) {
                    	if(selectedItems.size() > 0){
                    		int penalty = 0;
                    		if(selectedItems.contains(0)) penalty += 100;
                    		if(selectedItems.contains(1)) penalty += 100;
                    		if(selectedItems.contains(2)) penalty += 300;
                    		if(selectedItems.contains(3)) penalty += 100;
                    		
                    		String message = "Penalty|" + Integer.toString(ContestInfo.getCurNum()) + "|" + round + "|" + name + "|" + Integer.toString(penalty);  
                    		if(!TcpClient.sendMessage(message))
                    		{
                    			error = Toast.makeText(context, "Data sending error", duration);
            					error.setGravity(Gravity.CENTER, 0, 0);
            					error.show();
                    		}
                    		else
                    		{
                    			error = Toast.makeText(context, "Data was send succcessfully", duration);
            					error.setGravity(Gravity.CENTER, 0, 0);
            					error.show();
                    		}
                    	}
                    }
                });
				builder.setNegativeButton("No", new android.content.DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int arg1) {
                        
                    }
                });
				builder.setCancelable(true);
				builder.setOnCancelListener(new OnCancelListener() {
                    public void onCancel(DialogInterface dialog) {
                        
                    }
                });
				builder.show();		
			}
		});
        
        builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
			
			@Override
			public void onClick(DialogInterface dialog, int which) {
						
			}
		});
        Dialog dialog = builder.create();
        return dialog;
	}
}
