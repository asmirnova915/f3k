﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Office.Interop.Excel;

namespace Server
{
    public class ExcelParser
    {
        Application objExcel;
        Workbook objWorkbook;
        String file;
        System.Windows.Forms.ProgressBar prog;
        int progOld;
        public ExcelParser(String fileName, System.Windows.Forms.ProgressBar progress)
        {
            objExcel = GetApplication();
            objWorkbook = objExcel.Workbooks.Open(fileName);
            
            file = fileName;
            prog = progress;
            progOld = prog.Value;
        }

        Application GetApplication()
        {
            Application excel = null;
            try 
            {
               excel = System.Runtime.InteropServices.Marshal.GetActiveObject("Excel.Application") as Application;
            }
            catch(Exception ex)
            {
                excel = new Application();
            }
            return excel;
        }

        public ExcelParser()
        {
            objExcel = new Application();
        }

        public List<String> GetTaskList()
        {
            List<String> taskList = new List<String>();
            
            int sheetNum = -1;
            for (int i = 1; i <= objWorkbook.Sheets.Count; i++ )
            {
                if (objWorkbook.Sheets[i].Name == "Task List")
                {
                    sheetNum = i;
                    break;
                }
            }
            if (sheetNum == -1)
            {
                throw new System.ApplicationException("There is no TaskList sheet in this Excel book");
            }
            
            Worksheet objWorksheet = objWorkbook.Sheets[sheetNum];
            int rowNum = 2;
            String[] split;
            String task = (objWorksheet.Cells[rowNum, 2] as Range).Text;
            int step = prog.Maximum / 60;
            int sum = 0;
            int maxSum = prog.Maximum / 3;
            while(task != "")
            {
                split = task.Split(new Char[] { '\"' });
                taskList.Add(split[1]);
                rowNum++;
                task = (objWorksheet.Cells[rowNum, 2] as Range).Text;
                if(sum < maxSum)
                {
                    prog.Value += step;
                    sum += step;
                }
                
            }
            progOld += prog.Maximum / 3;
            prog.Value = progOld;
            return taskList;
        }

        public List<OneTask> GetRoundList(List<String> tasks)
        {
            List<OneTask> rounds = new List<OneTask>();
            String[] split;
            int curRound = -1;
            String curTask;
            String curGroup = "";
            String curName;
            Worksheet objWorksheet;
            int curJudge = 0;
            int rowNum;
            int step = 1;
            int sum = 0;
            int maxSum = prog.Maximum / 3;
            for (int i = 1; i <= objWorkbook.Sheets.Count; i++)
            {
                if(objWorkbook.Sheets[i].Name.IndexOf("Round") == 0)
                {
                    //лист содержит инф-ию о раунде.
                    //получаем номер раунда:
                    objWorksheet = objWorkbook.Sheets[i];
                    split = objWorksheet.Name.Split(new Char [] { '-' });
                    curRound = Convert.ToInt32(split[1]);
                    curTask = tasks[curRound - 1];
                    rowNum = 2;

                    curName = (objWorksheet.Cells[rowNum, 1] as Range).Text;
                    while(curName != "")
                    {
                        if (curName.IndexOf("#") == 0)
                        {//устанавливаем текущую группу
                            split = curName.Split(new Char[] { ' ' });
                            curGroup = split[4];
                            curJudge = 1;
                        }
                        else
                        {//создаем новый таск
                            curName = (objWorksheet.Cells[rowNum, 1] as Range).Text;
                            rounds.Add(new OneTask(curName,curTask,curRound,curJudge,curGroup));
                            curJudge++;
                            if (sum < maxSum)
                            {
                                prog.Value += step;
                                sum += step;
                            }
                        }
                        rowNum++;
                        curName = (objWorksheet.Cells[rowNum, 1] as Range).Text;
                    }
                }
            }
            progOld += prog.Maximum / 3;
            prog.Value = progOld;
            
            return rounds;
        }


        public void CloseBook()
        {
            objExcel.Quit();
        }
    }
}
