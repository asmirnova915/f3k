﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
namespace Server
{
    public class ContestInfo
    {
        public static List<String> tasks;
        public static List<OneTask> rounds;
        public static List<String> trackList;
        public static ProgressBar progress;
        public static Boolean contestIsRunning = false;
        private ContestInfo()
        {
            tasks = new List<String>();
            rounds = new List<OneTask>();
            trackList = new List<String>();
        }

        private static ContestInfo info = null;
        public static ContestInfo CreateContestInfo()
        {
            if(info == null) info = new ContestInfo();
            return info;
        }

        public void GetContestInfo(String fileName)
        {
            ExcelParser parser = null;
            try
            {
                parser = new ExcelParser(fileName, progress);
                tasks = parser.GetTaskList();
                rounds = parser.GetRoundList(tasks);
                if (ContestInfo.rounds.Count == 0)
                {
                    MessageBox.Show("Round list is empty.\nAre you shure that you've chosen the correct data file?");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message + "\nContest data isn't correct");

            }
            finally
            {
                if (parser != null) parser.CloseBook();
            }
        }

        public void GetTrackList(String fileName)
        {
            try
            {
                XmlParser parser = new XmlParser(fileName, progress);
                trackList = parser.parseTrackList();
                if (ContestInfo.trackList.Count == 0)
                {
                    MessageBox.Show("Playlist is empty.\nAre you shure that you've chosen the correct data file?");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message + "\nContest data isn't correct");
            }            
        }
    }
}
