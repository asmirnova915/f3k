package com.example.myclient;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

public class ConnectionActivity extends Activity {

	private TcpClient mTcpClient;
	private EditText ipAddressText;
	private Button connectButton;
	private EditText portText;
	private TextView messageText;
	private ProgressBar progressBar;
	private EditText numberText;
	
	private Handler handler;
	private Handler messageHandler;
	Context context;
	int duration;
	Toast error;
	Activity activity;
	String serverMessage;
	
	boolean rounds = false;
	boolean tracks = false;
	boolean curNum = false;
	ContestInfo info = ContestInfo.createContestInfo();
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_connection);
		
		ipAddressText = (EditText) this.findViewById(R.id.ip_edit);
		portText = (EditText) this.findViewById(R.id.port_edit);
		connectButton = (Button) this.findViewById(R.id.connect);
		messageText = (TextView) this.findViewById(R.id.servermessage);
		progressBar = (ProgressBar) this.findViewById(R.id.progressBar1);
		numberText = (EditText) this.findViewById(R.id.number_edit);
		context = getApplicationContext();
		duration = Toast.LENGTH_SHORT;
		activity = this;
		
		handler = new Handler()
		{
			public void handleMessage(Message msg)
			{
				switch(msg.what)
				{
				case TcpClient.CONNECTED_MESSAGE:
					progressBar.setVisibility(View.VISIBLE);
					progressBar.setIndeterminate(true);
					ContestInfo.rounds.clear();
					ContestInfo.playlist.clear();
					TcpClient.sendMessage("Info|" + numberText.getText().toString());
					break;
				case TcpClient.DATA_RECEIVED:
					messageText.setVisibility(View.INVISIBLE);
					progressBar.setVisibility(View.INVISIBLE);
					rounds = false;
					tracks = false;
					curNum = false;
					connectButton.setEnabled(true);
					ContestInfo.resultWasSent = true;
					ContestInfo.flights.clear();
					Log.e("playlist = ", Integer.toString(info.getPlaylist().size()));
					Intent intent = new Intent(activity,TabsActivity.class);
					startActivity(intent);
					break;
				case TcpClient.CONNECTION_ERROR:
					messageText.setVisibility(View.INVISIBLE);
					progressBar.setVisibility(View.INVISIBLE);
					error = Toast.makeText(context, "Connection error", duration);
					error.setGravity(Gravity.CENTER, 0, 0);
					error.show();
					connectButton.setEnabled(true);
					break;
				case TcpClient.CONNECTING_MESSAGE:
					progressBar.setVisibility(View.VISIBLE);
					progressBar.setIndeterminate(true);
					messageText.setText("Connecting...");
					messageText.setVisibility(View.VISIBLE);
					break;
				case TcpClient.CONTEST_IS_RUNNING:
					messageText.setVisibility(View.INVISIBLE);
					progressBar.setVisibility(View.INVISIBLE);
					error = Toast.makeText(context, "Contest is running now. Try again later.", duration);
					mTcpClient.stopClient();
					error.setGravity(Gravity.CENTER, 0, 0);
					error.show();
					connectButton.setEnabled(true);
					break;
					
				}
			}
		};
		PreferenceManager.setDefaultValues(this, R.xml.preferences, false);
			 
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		//getMenuInflater().inflate(R.menu.connection, menu);
		return true;
	}
	
	
	
	public void onButtonClick(View view)
	{
		
	    connectButton.setEnabled(false);
		
		try{
			String address = ipAddressText.getText().toString();
			int port = Integer.parseInt(portText.getText().toString());
			if(!TcpClient.setServerAddress(address, port))
			{
				error = Toast.makeText(context, "Wrong address format", duration);
				error.setGravity(Gravity.CENTER, 0, 0);
				error.show();
				connectButton.setEnabled(true);	
				return;
			}
			new connectTask().execute("");
		} 
		catch(Exception ex){
			error = Toast.makeText(context, "Wrong address format", duration);
			error.setGravity(Gravity.CENTER, 0, 0);
			error.show();
			connectButton.setEnabled(true);
		}
	    	
			
	}
	public void onStart(){
		super.onStart();
		TcpClient.setHandler(handler);
	}

	public class connectTask extends AsyncTask<String,String,TcpClient> {
	
		protected TcpClient doInBackground(String... message) {			
			
            //we create a TCPClient object and
			 mTcpClient = TcpClient.createTcpClient(new TcpClient.OnMessageReceived() {
	            	
	                @Override
	                //here the messageReceived method is implemented
	                public void messageReceived(String message) {
	                	if(message.equals("Error")) {
	                		handler.sendEmptyMessage(TcpClient.CONTEST_IS_RUNNING);
	                		return;
	                	}
	                	
	                    if(!curNum) {
	                    	info.setCurNum(message);
	                    	curNum = true;
	                    	return;
	                    }
	                    else if(message.equals("TaskList")){
	                    	rounds = true;
	                    	return;
	                    }
	                    else if(message.equals("PlayList"))
	                    {
	                    	tracks = true;
	                    	return;
	                    }
	                    else if(message.equals("<end>")){
	                    	if(tracks) handler.sendEmptyMessage(TcpClient.DATA_RECEIVED);
	                    	else if(rounds) rounds = false;
	                    	return;
	                    }
	                    else if(rounds) {
	                    	Log.e("Round", message);
	                    	info.addRound(message);
	                    	return;
	                    }
	                    info.addTrack(message);
	                }
	            });
            
            mTcpClient.run(); 
               
            return null;
        }
	}
}
