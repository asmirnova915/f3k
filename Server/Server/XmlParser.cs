﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.IO;

namespace Server
{
    class XmlParser
    {
        String fileName;
        List<Tracks> tracks;
        System.Windows.Forms.ProgressBar prog;
        static Char[] split1 = new Char[] { '/' };
        static Char[] split2 = new Char[] { '.' };

        
        struct Tracks
        {
            public int id;
            public String name;
            public bool exist;

            public Tracks(int i, String n)
            {
                id = i;
                name = (String)n.Clone();
                String[] split = name.Split(split2);
                if (split.Length > 1) exist = true;
                else exist = false;
            }
        }

        public XmlParser(String file, System.Windows.Forms.ProgressBar progress)
        {
            fileName = file;
            prog = progress;
        }

        public List<String> parseTrackList()
        {
            List<String> trackList = new List<String>();
            tracks = new List<Tracks>();
            int sumMax = prog.Maximum / 3;
            int sum = 0;
            StreamReader readerSR = new StreamReader(fileName, Encoding.GetEncoding(1251));
            XmlTextReader reader = new XmlTextReader(readerSR);
            reader.WhitespaceHandling = WhitespaceHandling.None;
            int step = prog.Maximum/300;
            bool t = false;
            while (reader.Read())
            {
                if (reader.NodeType == XmlNodeType.Text)
                    if (reader.Value == "Tracks")
                    {
                        t = true;
                        break;
                    }
            }
            if (!t)
            {
                reader.Close();
                return trackList;
            }

            bool p = false;
            int trID = 0;
            String trName;
            String[] split;
            while (reader.Read())
            {
                if (reader.NodeType == XmlNodeType.Text)
                {
                    if (reader.Value == "Playlists")
                    {
                        p = true;
                        break;
                    }
                    else if (reader.Value == "Track ID")
                    {
                        reader.Read();
                        reader.ReadEndElement();
                        trID = reader.ReadElementContentAsInt();
                    }
                    else if (reader.Value == "Location")
                    {
                        reader.Read();
                        reader.ReadEndElement();
                        trName = reader.ReadElementContentAsString().Split(split1).Last();
                        tracks.Add(new Tracks(trID, trName));
                        if (sum < sumMax)
                        {
                            sum += step;
                            prog.Value += step;
                        }
                    }
                }
            }
            if (!p || tracks.Count < 1)
            {
                reader.Close();
                return trackList;
            };

            //считываем непосредственно плейлист. файлы, где нет .mp3 в расчет не берем - их нет
            int curNum;
            while (reader.Read())
            { 
                if(reader.NodeType == XmlNodeType.Element)
                {
                    if (reader.Name == "integer")
                    {
                        curNum = reader.ReadElementContentAsInt() - tracks[0].id;
                        if (tracks[curNum].exist) trackList.Add(tracks[curNum].name);
                        if (sum < sumMax)
                        {
                            sum += step;
                            prog.Value += step;
                        }
                    }
                }
            }
            reader.Close();
            readerSR.Close();
            return trackList;
        }
    }
}
