package com.example.myclient.adapter;


import java.util.ArrayList;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.example.myclient.R;

public class ListViewAdapter extends ArrayAdapter<String> {
	private final Context context;
	private final ArrayList<String> values;
	
	public ListViewAdapter(Context context, ArrayList<String> values){
		super(context, R.layout.list_elem, values);
		this.context = context;
		this.values = values;
	}
	
	@Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View rowView = inflater.inflate(R.layout.list_elem, parent, false);
        TextView roundTime = (TextView) rowView.findViewById(R.id.round_time);
        TextView roundNum = (TextView) rowView.findViewById(R.id.round_num);
        roundTime.setText(values.get(position));
        roundNum.setText("Flight �" + Integer.toString(position+1));

        return rowView;
    }
}
