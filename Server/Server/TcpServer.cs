﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Net;
using System.IO;
using System.Net.Sockets;
using System.Threading;

namespace Server
{
    
    class TcpServer
    {
        public static Hashtable htConnections = new Hashtable(30);
        public static Hashtable htUsers = new Hashtable(30);
        private TcpClient tcpClient;
        private int port;
        static private TextBox textbox;
        static private Form1 form1;
        static public ExcelWriter writer;

        delegate void SetTextCallback(string text);

        public TcpServer(int p, TextBox t, Form1 f, String file)
        {
            port = p;
            textbox = t;
            form1 = f;
            writer = new ExcelWriter(file);
        }

        // The thread that will hold the connection listener
        private Thread thrListener;

        // The TCP object that listens for connections
        private TcpListener tlsClient;

        // Will tell the while loop to keep monitoring for connections
        bool ServRunning = false;

        public static void AddUser(TcpClient tcpUser, int userNum)
        {
            TcpServer.htUsers.Add(userNum, tcpUser);
            TcpServer.htConnections.Add(tcpUser, userNum);
            PrintClientsList();
        }

        public static void RemoveUser(TcpClient tcpUser)
        {
            if(htConnections[tcpUser] != null)
            {
                TcpServer.htUsers.Remove(TcpServer.htConnections[tcpUser]);
                TcpServer.htConnections.Remove(tcpUser);
                PrintClientsList();
            }
        }

        // This is called when we want to raise the StatusChanged event

        static void PrintClientsList()
        {
            String text = "";
            foreach(DictionaryEntry elem in htUsers)
            {
                text += elem.Key.ToString() + " ";
            }
            SetText(text);
        }

        static void SetText(String text)
        {

            if (textbox.InvokeRequired)
            {
                SetTextCallback d = new SetTextCallback(SetText);
                form1.Invoke(d, new object[] { text });
            }
            else
            {
                textbox.Text = text;
            }
        }

        public void SendMessageToAllClients(string Message)
        {
            StreamWriter swSender;
          
            TcpClient[] tcpClients = new TcpClient[TcpServer.htUsers.Count];
            TcpServer.htUsers.Values.CopyTo(tcpClients, 0);

            for (int i = 0; i < tcpClients.Length; i++)
            {
                try 
                {
                    if(tcpClients[i] == null)
                    {
                        continue;
                    }

                    swSender = new StreamWriter(tcpClients[i].GetStream());
                    swSender.WriteLine(Message);
                    swSender.Flush();
                    swSender = null;
                }
                catch
                {
                    RemoveUser(tcpClients[i]);
                }
            }
        }

        public void StartListening()
        {
            tlsClient = new TcpListener(IPAddress.Any, port);
            tlsClient.Start();
            ServRunning = true;
            thrListener = new Thread(KeepListening);
            thrListener.Start();
        }

        private void KeepListening()
        {
            while(ServRunning)
            {
                try 
                {
                    tcpClient = tlsClient.AcceptTcpClient();
                    Connection newConnection = new Connection(tcpClient);
                }
                catch(Exception ex)
                {
                    break;
                }
            }
        }

        public void StopServer()
        {
            writer.CloseBook();
            ServRunning = false;
            SendMessageToAllClients("Shut down");
            while (htUsers.Count > 0) ;
            tlsClient.Stop();
            thrListener.Abort();
            
        }
    }

    class Connection
    {
        TcpClient tcpClient;

        // The thread that will send information to the client
        private Thread thrSender;
        private StreamReader srReceiver;
        private StreamWriter swSender;
        private int currUser;
        private string strResponse;

        // The constructor of the class takes in a TCP connection
        public Connection(TcpClient tcpCon)
        {
            tcpClient = tcpCon;
            thrSender = new Thread(AcceptClient);
            thrSender.Start();
        }

        private int findFreeNumber()
        {
            for (int i = 1; i <= 30; i++)
            {
                if (!TcpServer.htUsers.Contains(i))
                {
                    return i;                   
                }
            }
            return -1;
        }
        private int setClientNum(String num) 
        {
            int number;
            try
            {
                number = Convert.ToInt32(num, 10);
                if (TcpServer.htUsers.Contains(number)) return findFreeNumber();
                else return number;
            }
            catch (Exception ex)
            {
                return findFreeNumber();
            }
        }
        private void AcceptClient()
        {
            srReceiver = new StreamReader(tcpClient.GetStream());
            swSender = new StreamWriter(tcpClient.GetStream());
            currUser = -1;
            try
            {

            String userMessage = srReceiver.ReadLine();

            if(userMessage.IndexOf("Info") > -1)
            {
                if (ContestInfo.contestIsRunning) 
                {
                    swSender.WriteLine("Error");
                    swSender.Flush();
                    CloseConnection();
                    return;
                }
                String [] split = userMessage.Split(new Char[]{'|'});
                currUser = setClientNum(split[1]);
                
                if (currUser == -1)
                {
                    swSender.WriteLine("Connection error. Too many users");
                    swSender.Flush();
                    CloseConnection();  
                    return;                      
                }
                else
                {
                    swSender.WriteLine(currUser.ToString());
                    swSender.Flush();
                    String message = "TaskList";
                    swSender.WriteLine(message);
                    swSender.Flush();
                    //send task list
                    foreach (OneTask task in ContestInfo.rounds)
                    {
                        if (task.getJudgeNum() == currUser)
                        {
                            swSender.WriteLine(task.ToString());
                            swSender.Flush();
                        }                        
                    }
                    swSender.WriteLine("<end>");
                    swSender.Flush();
                    
                    message = "PlayList";
                    swSender.WriteLine(message);
                    swSender.Flush();
                    foreach (String task in ContestInfo.trackList)
                    {
                        swSender.WriteLine(task);
                        swSender.Flush();
                    }
                    swSender.WriteLine("<end>");
                    swSender.Flush();
                    TcpServer.AddUser(tcpClient, currUser);
               }             
               
            }            

           
                // Keep waiting for a message from the user
                while ((strResponse = srReceiver.ReadLine()) != "")
                {
                    // If it's invalid, remove the user
                    if (strResponse == null)
                    {
                        TcpServer.RemoveUser(tcpClient);
                        CloseConnection();
                    }
                    else
                    {
                        //work with client  
                        if(strResponse.IndexOf("Logoff") > -1)
                        {
                            TcpServer.RemoveUser(tcpClient);
                            CloseConnection();
                            return;
                        }
                        else if (strResponse.IndexOf("Res") > -1)
                        {
                           // MessageBox.Show(strResponse);
                            TcpServer.writer.writeResult(strResponse);
                        }
                        else if(strResponse.IndexOf("Penalty") > -1)
                        {
                            TcpServer.writer.writePenalty(strResponse);
                        }
                    }
                }
            }
            catch
            {
                // If anything went wrong with this user, disconnect him
                TcpServer.RemoveUser(tcpClient);
                CloseConnection();
            }

        }

        private void CloseConnection()
        {
            tcpClient.Close();
            srReceiver.Close();
            swSender.Close();            
        }
    }
}
