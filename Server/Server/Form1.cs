﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Server
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            contest_gb.Enabled = false;
            server_gb.Enabled = false;
            get_data_b.Enabled = false;
            tablesAreKnown = false;
            playlistIsKnown = false;
        }

        bool dataIsKnown;
        bool tablesAreKnown;
        bool playlistIsKnown;
        ContestInfo contest;
        TcpServer server = null;

        private void tables_browse_b_Click(object sender, EventArgs e)
        {
            //get contest data
            openFileDialog = new OpenFileDialog();
            openFileDialog.Filter = "Excel files (.xlsx)|*.xlsx|All Files (*.*)|*.*";
            openFileDialog.FilterIndex = 1;
            if (openFileDialog.ShowDialog() == DialogResult.OK)
            {
                if (openFileDialog.FileName.Split(new Char[] { '.' }).Last() != "xlsx")
                {
                    MessageBox.Show("Wrong file format.\nTry to choose Excel file.");
                    return;
                }
                tables_location_tb.Text = openFileDialog.FileName;
                tablesAreKnown = true;
                if (playlistIsKnown) get_data_b.Enabled = true;
            }
        }

        private void playlist_brouse_b_Click(object sender, EventArgs e)
        {
            openFileDialog = new OpenFileDialog();
            openFileDialog.Filter = "Xml files (.xml)|*.xml|All Files (*.*)|*.*";
            openFileDialog.FilterIndex = 1;
            if (openFileDialog.ShowDialog() == DialogResult.OK)
            {
                if (openFileDialog.FileName.Split(new Char[] { '.' }).Last() != "xml")
                {
                    MessageBox.Show("Wrong file format.\nTry to choose Xml file.");
                    return;
                }
                /* ExcelParser parser = new ExcelParser(openFileDialog.FileName);
                 contest.GetContestInfo(openFileDialog.FileName);
                 if (ContestInfo.rounds.Count == null)
                 {
                     MessageBox.Show("Round list is empty.\nAre you shure that you've chosen the correct data file?");
                 }*/
                playlist_location_tb.Text = openFileDialog.FileName;
                playlistIsKnown = true;
                if (tablesAreKnown) get_data_b.Enabled = true;
            }
        }

        private void get_data_b_Click(object sender, EventArgs e)
        {
            progressBar1.Value = progressBar1.Minimum;
            ContestInfo.progress = this.progressBar1;
            contest = ContestInfo.CreateContestInfo();
            contest.GetContestInfo(tables_location_tb.Text);
            contest.GetTrackList(playlist_location_tb.Text);
            progressBar1.Value = progressBar1.Maximum;
            if(ContestInfo.rounds.Count > 0 && ContestInfo.trackList.Count > 0) server_gb.Enabled = true;
        }

        private void start_server_b_Click(object sender, EventArgs e)
        {
            try
            {
                server = new TcpServer((int)server_port.Value, this.clients_tb, this, tables_location_tb.Text);
                server.StartListening();
                server_status.Text = "running";
                stop_server_b.Enabled = true;
                start_server_b.Enabled = false;
                contest_data_gb.Enabled = false;
                contest_gb.Enabled = true;
                server_port.Enabled = false;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void stop_server_b_Click(object sender, EventArgs e)
        {
            server.StopServer();
            server_status.Text = "stopped";
            start_contest_b.Text = "Shut down";
            stop_server_b.Enabled = false;
            start_server_b.Enabled = true;
            contest_data_gb.Enabled = true;
            contest_gb.Enabled = false;
            server_port.Enabled = true;
        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (server != null) server.StopServer();
        }

        private void start_contest_b_Click(object sender, EventArgs e)
        {
            if (start_contest_b.Text == "Start contest")
            {
                Char[] groupChar = new Char[] { (char)('A' + (int)group_nud.Value - 1) };
                String group = new String(groupChar);
                server.SendMessageToAllClients("Start|" + round_nud.Value.ToString() + "|" + group);
                start_contest_b.Text = "Stop contest";
                ContestInfo.contestIsRunning = true;
            }
            else
            {
                server.SendMessageToAllClients("Stop");
                start_contest_b.Text = "Start contest";
                ContestInfo.contestIsRunning = false;
            }
            
        }

        private void pause_contest_b_Click(object sender, EventArgs e)
        {
            if (server_status.Text == "running")
            {//pause
                server.SendMessageToAllClients("Pause");
                server_status.Text = "on pause";
                pause_contest_b.Text = "Resume contest";
            }
            else 
            {
                server.SendMessageToAllClients("Resume");
                server_status.Text = "running";
                pause_contest_b.Text = "Pause contest";
            }
        }

       
    }
}
